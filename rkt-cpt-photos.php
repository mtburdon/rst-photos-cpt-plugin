<?php
/**
* Plugin Name: Rocketship Themes Photos Custom Post Type
* Plugin URI: http://rocketshipthemes.co.uk
* Description: Photos Custom Post Type
* Version: 2.0.1
* Author: Rocketship Themes - Martin Burdon
* Author URI: http://rocketshipthemes.co.uk
*/

add_action('plugins_loaded', 'rkt_photos_load_textdomain');
function rkt_photos_load_textdomain() {
  load_plugin_textdomain( 'rkt-cpt-photos', false, dirname( plugin_basename(__FILE__) ) . '/language/' );
}

/*
 *  Register the Custom Photos Post Type
 */
function rkt_register_cpt_photos() {
  $labels = array(
    'name' => esc_html__( 'Photos', 'rkt-cpt-photos' ),
    'singular_name' => esc_html__( 'Photo', 'rkt-cpt-photos' ),
    'add_new' => esc_html__( 'Add New', 'rkt-cpt-photos' ),
    'add_new_item' => esc_html__( 'Add New Photo', 'rkt-cpt-photos' ),
    'edit_item' => esc_html__( 'Edit Photo', 'rkt-cpt-photos' ),
    'new_item' => esc_html__( 'New Photo', 'rkt-cpt-photos' ),
    'view_item' => esc_html__( 'View Photo', 'rkt-cpt-photos' ),
    'search_items' => esc_html__( 'Search Photos', 'rkt-cpt-photos' ),
    'not_found' => esc_html__( 'No photos found', 'rkt-cpt-photos' ),
    'not_found_in_trash' => esc_html__( 'No photos found in Trash', 'rkt-cpt-photos' ),
    'parent_item_colon' => esc_html__( 'Parent Photo:', 'rkt-cpt-photos' ),
    'menu_name' => esc_html__( 'Photos', 'rkt-cpt-photos' ),
  );

  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'description' => esc_html__( 'Collection of Photos', 'rkt-cpt-photos' ),
    'supports' => array( 'title', 'author', 'thumbnail', 'revisions' ),
    'taxonomies' => array( 'album' ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'menu_icon' => 'dashicons-images-alt',
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => true,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
    'capability_type' => 'post'
  );

  register_post_type( 'rkt_photos', $args );
}

add_action( 'init', 'rkt_register_cpt_photos' );

/*
 *  Add custom meta box for description
 */
add_action( 'admin_init', 'rkt_photos_description_meta' );
function rkt_photos_description_meta() {
  add_meta_box(
    'rkt_photos_description',               // HTML ID attribute
    esc_html__( 'Photo Description', 'rkt-cpt-photos' ),        // Meta box heading
    'rkt_display_photos_description_meta',  // Callback which renders the contents of the meta box
    'rkt_photos',                           // Custom post type where the meta box should be displayed
    'normal',                               // Which part of the screen the meta box should sit
    'high'                                  // Priority within the context where the boxes should show
  );
}

function rkt_display_photos_description_meta( $photo ) {
  // Retrieve current description on review ID
  $photo_description = esc_html( get_post_meta( $photo->ID, 'rkt_photo_description', true ) ); ?>
  <table>
    <tr>
      <td><textarea rows="4" cols="50" name="rkt_photo_description_text"><?php echo esc_html($photo_description); ?></textarea></td>
    </tr>
  </table>
  <?php
}

add_action( 'save_post', 'rkt_add_photo_fields', 10, 2 );
function rkt_add_photo_fields( $photo_id, $photo ) {
  // Check post type for photos
  if ( $photo->post_type == 'rkt_photos' ) {
    // Store data in post meta table if present in post data
    if ( isset( $_POST['rkt_photo_description_text'] ) && $_POST['rkt_photo_description_text'] != '' ) {
      update_post_meta( $photo_id, 'rkt_photo_description', $_POST['rkt_photo_description_text'] );
    }
  }
}

/*
 *  Create the custom taxonomy
 */
function rkt_album_taxonomy() {
  register_taxonomy(
    'album',
    'rkt_photos',
    array(
      'hierarchical' => false,
      'label' => esc_html__( 'Photo Albums', 'rkt-cpt-photos' ),
      'query_var' => true,
      'rewrite' => array(
        'slug' => 'album',
        'with_front' => false
      )
    )
  );
}
add_action( 'init', 'rkt_album_taxonomy');

/*
 *  Add extra colum to the list of photos in the admin area
 */
add_filter( 'manage_rkt_photos_posts_columns', 'rkt_photos_cols' );
function rkt_photos_cols( $columns ) {
  unset( $columns['title'] );
  unset( $columns['author'] );
  unset( $columns['date'] );
  $columns['rkt_image'] = 'Image';
  $columns['title'] = 'Title';
  $columns['rkt_description'] = 'Description';
  $columns['date'] = 'Date';
  return $columns;
}

add_action( 'manage_rkt_photos_posts_custom_column', 'rkt_photos_populate_columns' );
function rkt_photos_populate_columns( $column ) {
  if ( 'rkt_description' == $column ) {
    echo esc_html( get_post_meta( get_the_ID(), 'rkt_photo_description', true ) );
  }
  elseif ( 'rkt_image' == $column ) {
    echo the_post_thumbnail( 'thumbnail' );
  }
}

/*
 *  Overwrite the in-theme location of the shortcodes to use this plugin directory
 */
function rkt_unyson_shortcodes_photos($locations) {
  $locations[dirname(__FILE__) . '/extensions'] = plugin_dir_url( __FILE__ ) . 'extensions';
  return $locations;
}

add_filter('fw_extensions_locations', 'rkt_unyson_shortcodes_photos');
