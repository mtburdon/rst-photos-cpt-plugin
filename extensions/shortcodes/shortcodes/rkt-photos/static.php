<?php if (!defined('FW')) die('Forbidden');

$uri = plugin_dir_url( __FILE__ );

wp_enqueue_script(
  'fw-lightbox-js',
  $uri . '/static/js/lightbox.min.js',
  array('jquery'),
  '2.7.1',
  true
);

wp_enqueue_script(
  'fw-owl-slider-js',
  $uri . '/static/js/owl.carousel.min.js',
  array('jquery'),
  '1.3.3',
  true
);

wp_enqueue_script(
  'fw-macy-js',
  $uri . '/static/js/macy.min-2.0.0.js',
  array('jquery'),
  '2.0.0',
  true
);
