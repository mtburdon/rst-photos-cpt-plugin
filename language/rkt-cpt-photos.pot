#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: rkt-cpt-photos\n"
"POT-Creation-Date: 2017-03-30 22:06+0100\n"
"PO-Revision-Date: 2015-07-22 13:45+0100\n"
"Last-Translator: \n"
"Language-Team: Rocketship Themes <support@rocketshipthemes.co.uk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.6\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: en\n"
"X-Poedit-KeywordsList: __;_e;esc_html__;esc_html_e\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: .\n"

#: extensions/shortcodes/shortcodes/rkt-photos/config.php:5
#: rkt-cpt-photos.php:21 rkt-cpt-photos.php:32
msgid "Photos"
msgstr ""

#: extensions/shortcodes/shortcodes/rkt-photos/config.php:6
msgid "Add a collection of photos"
msgstr ""

#: extensions/shortcodes/shortcodes/rkt-photos/config.php:7
msgid "Rocketship Themes"
msgstr ""

#: extensions/shortcodes/shortcodes/rkt-photos/options.php:13
msgid "Album(s)"
msgstr ""

#: extensions/shortcodes/shortcodes/rkt-photos/options.php:14
msgid "Select which photo albums to display"
msgstr ""

#: extensions/shortcodes/shortcodes/rkt-photos/options.php:15
msgid "When creating your photos you can add a tag to each one in order to group them into albums. Select which albums you would like to display."
msgstr ""

#: extensions/shortcodes/shortcodes/rkt-photos/options.php:27
msgid "Layout style"
msgstr ""

#: extensions/shortcodes/shortcodes/rkt-photos/options.php:28
msgid "Choose the layout of your photo album"
msgstr ""

#: extensions/shortcodes/shortcodes/rkt-photos/options.php:64
msgid "Pagination"
msgstr ""

#: extensions/shortcodes/shortcodes/rkt-photos/options.php:65
msgid "Turn pagination on or off"
msgstr ""

#: extensions/shortcodes/shortcodes/rkt-photos/options.php:68
msgid "On"
msgstr ""

#: extensions/shortcodes/shortcodes/rkt-photos/options.php:72
msgid "Off"
msgstr ""

#: extensions/shortcodes/shortcodes/rkt-photos/options.php:78
msgid "Autoplay Interval"
msgstr ""

#: extensions/shortcodes/shortcodes/rkt-photos/options.php:79
msgid "How many seconds should pass between scrolling to the next image"
msgstr ""

#: extensions/shortcodes/shortcodes/rkt-photos/options.php:93
msgid "Margin"
msgstr ""

#: extensions/shortcodes/shortcodes/rkt-photos/options.php:94
msgid "Margin between items"
msgstr ""

#: rkt-cpt-photos.php:22
msgid "Photo"
msgstr ""

#: rkt-cpt-photos.php:23
msgid "Add New"
msgstr ""

#: rkt-cpt-photos.php:24
msgid "Add New Photo"
msgstr ""

#: rkt-cpt-photos.php:25
msgid "Edit Photo"
msgstr ""

#: rkt-cpt-photos.php:26
msgid "New Photo"
msgstr ""

#: rkt-cpt-photos.php:27
msgid "View Photo"
msgstr ""

#: rkt-cpt-photos.php:28
msgid "Search Photos"
msgstr ""

#: rkt-cpt-photos.php:29
msgid "No photos found"
msgstr ""

#: rkt-cpt-photos.php:30
msgid "No photos found in Trash"
msgstr ""

#: rkt-cpt-photos.php:31
msgid "Parent Photo:"
msgstr ""

#: rkt-cpt-photos.php:38
msgid "Collection of Photos"
msgstr ""

#: rkt-cpt-photos.php:68
msgid "Photo Description"
msgstr ""

#: rkt-cpt-photos.php:107
msgid "Photo Albums"
msgstr ""
