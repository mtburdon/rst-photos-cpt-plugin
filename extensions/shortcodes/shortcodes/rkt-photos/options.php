<?php if (!defined('FW')) die('Forbidden');

$plugin_dir = plugin_dir_url( __FILE__ );
$albumTermsArr = get_terms('album');
$albumNames = array();
foreach ($albumTermsArr as &$value) {
  $albumNames[$value->term_id] = $value->slug;
}

$options = array(
  'album' => array(
    'type'  => 'multi-select',
    'label' => esc_html__('Album(s)', 'rkt-cpt-photos'),
    'desc' => esc_html__('Select which photo albums to display', 'rkt-cpt-photos'),
    'help' => esc_html__('When creating your photos you can add a tag to each one in order to group them into albums. Select which albums you would like to display.', 'rkt-cpt-photos'),
    'population' => 'array',
    'choices' => $albumNames,
    'prepopulation' => 10,
    'limit' => 100
  ),
  'layout' => array(
    'type'    => 'multi-picker',
    'label'   => false,
    'desc'    => false,
    'picker'  => array(
      'layout_picker' => array(
        'label' => esc_html__( 'Layout style', 'rkt-cpt-photos' ),
        'desc' => esc_html__( 'Choose the layout of your photo album', 'rkt-cpt-photos' ),
        'type' => 'image-picker',
        'choices' => array(
          'layout-1' => array(
            'small' => array(
              'height' => 70,
              'src'    => $plugin_dir . 'static/img/image-picker/layout-1-thumb.png'
            )
          ),
          'layout-2' => array(
            'small' => array(
              'height' => 70,
              'src'    => $plugin_dir . 'static/img/image-picker/layout-2-thumb.png'
            )
          ),
          'layout-3' => array(
            'small' => array(
              'height' => 70,
              'src'    => $plugin_dir . 'static/img/image-picker/layout-3-thumb.png'
            )
          ),
          'layout-4' => array(
            'small' => array(
              'height' => 70,
              'src'    => $plugin_dir . 'static/img/image-picker/layout-4-thumb.png'
            )
          )
        )
      )
    ),
    'choices' => array(
      'layout-1' => array(),
      'layout-2' => array(
        'pagination' => array(
          'type'  => 'switch',
          'value' => 'off',
          'label' => esc_html__('Pagination', 'rkt-cpt-photos'),
          'desc'  => esc_html__('Turn pagination on or off', 'rkt-cpt-photos'),
          'left-choice' => array(
              'value' => 'on',
              'label' => esc_html__('On', 'rkt-cpt-photos')
          ),
          'right-choice' => array(
            'value' => 'off',
            'label' => esc_html__('Off', 'rkt-cpt-photos')
          ),
        ),
        'autoplay_interval' => array(
          'type' => 'slider',
          'value' => 3,
          'label' => esc_html__('Autoplay Interval', 'rkt-cpt-photos'),
          'desc'  => esc_html__('How many seconds should pass between scrolling to the next image', 'rkt-cpt-photos'),
          'properties' => array(
            'min' => 1,
            'max' => 10,
            'sep' => 1,
            'grid_snap' => true
          )
        )
      ),
      'layout-3' => array(),
      'layout-4' => array(
        'masonry_margin' => array(
          'type' => 'text',
          'value' => 20,
          'label' => esc_html__('Margin', 'rkt-cpt-photos'),
          'desc'  => esc_html__('Margin between items', 'rkt-cpt-photos')
        )
      )
    )
  )
);
