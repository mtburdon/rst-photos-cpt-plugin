<?php if (!defined('FW')) die('Forbidden');

    if (empty( $atts['album'] )) {
        return;
    }

    $layout = $atts['layout']['layout_picker'];
    $pagination = false; // Used in layout-2
    $transitionInterval = 3000; // Used in layout-2
    $masonryMargin = 20; // Used in layout-4
    $albumArr = $atts['album'];
    $selectedAlbums = '';

    foreach ($albumArr as &$value) {
        $term = get_term( $value, 'album');
        $slug = $term->slug;
        $selectedAlbums .= $slug . ', ';
    }
    // Remove the last comma/space
    $selectedAlbums = substr($selectedAlbums, 0, -2);

    $uid = "id_" . base_convert(microtime(), 10, 36);

    $extra_classes = '';
    if ($layout === 'layout-2') {
        $extra_classes = 'owl-carousel';
        $transitionInterval = ((int)$atts['layout']['layout-2']['autoplay_interval'] * 1000);

        if ( $atts['layout']['layout-2']['pagination'] === 'on' ) {
            $pagination = true;
        }
    } else if ($layout === 'layout-1') {
        $extra_classes = 'grid';
    } else if ($layout === 'layout-3') {
        $extra_classes = 'grid grid-alt';
    } else if ($layout === 'layout-4') {
        $extra_classes = 'masonry';
        $masonryMargin = (int)$atts['layout']['layout-4']['masonry_margin'];
    }
?>

<?php if ($layout === 'layout-2')  { ?>
    <script>
      jQuery(function() {
        jQuery(".rkt-photos").owlCarousel({
            autoPlay: "<?php echo intval($transitionInterval) ?>",
            pagination: !!"<?php echo esc_attr($pagination) ?>",
            items: 4,
            itemsDesktop: [1200, 3],
            itemsDesktopSmall: [979, 2]
        });
      });
    </script>
<?php } ?>

<?php if ($layout === 'layout-4')  { ?>
    <script>
      jQuery(function() {
        var config = {
          columns: 4,
          breakAt: {
            1300: 3,
            820: 2,
            520: 1
          }
        };

        if (window.config && window.config.cptPhotos && window.config.cptPhotos.masonry) {
          config = window.config.cptPhotos.masonry;
        }

        var MMM = Macy({
          container: '#container_<?php echo sanitize_html_class($uid) ?>',
          columns: config.columns,
          trueOrder: false,
          margin: <?php echo intval($masonryMargin) ?>,
          breakAt: config.breakAt
        });

        window.addEventListener('resize', function () {
          setTimeout(function () {
            MMM.reInit();
          }, 100);
        });
      });
    </script>
<?php } ?>
<div id="container_<?php echo sanitize_html_class($uid) ?>" class="rkt-photos <?php echo esc_attr($extra_classes) ?>">
    <?php
        $mypost = array( 'post_type' => 'rkt_photos', 'album' => $selectedAlbums, 'posts_per_page' => -1 );
        $loop = new WP_Query( $mypost );

        while ( $loop->have_posts() ) : $loop->the_post();
            // https://css-tricks.com/snippets/wordpress/get-featured-image-url/
            $thumb_id = get_post_thumbnail_id();
            $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
            $thumb_url = $thumb_url_array[0]; ?>
            <figure class="photo-item" data-sr="move 10px over 1s scale up 10% vFactor 0.40">
                <a href="<?php echo esc_url($thumb_url); ?>" data-lightbox="<?php echo sanitize_html_class($uid) ?>" data-title="<?php echo esc_html( get_post_meta( get_the_ID(), 'rkt_photo_description', true ) ); ?>">
                    <?php the_post_thumbnail( 'confettiparent_photo_thumb', array( 'class' => 'photo-img' ) ); ?>
                    <div class="photo-info">
                        <div class="photo-info-inner">
                            <h3 class="photo-title"><?php the_title(); ?></h3>
                        </div>
                    </div>
                </a>
            </figure>
            <?php
        endwhile;
    ?>
</div>
<?php wp_reset_query(); ?>
