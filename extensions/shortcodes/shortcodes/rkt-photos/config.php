<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Photos', 'rkt-cpt-photos' ),
		'description' => esc_html__( 'Add a collection of photos', 'rkt-cpt-photos'),
		'tab'         => esc_html__( 'Rocketship Themes', 'rkt-cpt-photos' )
	)
);